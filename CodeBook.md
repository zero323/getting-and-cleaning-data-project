Full description of the dataset: [Human Activity Recognition Using Smartphones Data Set](http://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones)

1. Data from the test dataset (`'test/X_test.txt'`) and the train dataset (`'train/X_train.txt'`) have been loaded into dataframes and combined using `rbind`
2. Activity labels from  `'activity_labels.txt'` have been loaded into dataframe 
3. Activity labels have been assigned to the columns of the combined data
4. Subset of the variables of interest created using `grep` command has been used to select columns containing mean ands tandard deviation columns from combined datasets
5. Lists of the subjects `'test/subject_test.txt'` and `'train/subject_train.txt'` have been combined and assigned to correct rows of the combined dataset.
6. Activity labels for test (`'test/y_test.txt'`) and train (`'train/y_train.txt'`) have been combined and merged with desciptive labels from `'activity_labels.txt'`. After that descriptive labels have been assigned to the rows of the combined data. Result dataframe has been assigned to a variable called `combined.data`
7. `combined.data` dataframe has been aggregated to obtain average values of each variable for each activity and each subject and assigned to a variable called `tidy.data`
8. Both `combined.data` and `tidy.data` as well as variable `date.obtained` (see [README.md](https://github.com/zero323/GettingAndCleaningDataProject/blob/master/README.md) for details) will be returned form a `main()` function.
