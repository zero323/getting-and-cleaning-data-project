Coursera Getting and Cleaning Data Project
========================================================
Usage:
------

Simply execute `main()` function from `run_analysis.R`.


```r
source("run_analysis.R")
data <- main()
names(data)
```

```
## [1] "tidy.data"     "combined.data" "date.obtained"
```

It will return list with three fields:
 - `combined.data` - combined test and train datasets
 - `tidy.data` - tidy data set with the average of each variable for each activity and each subject
 - `date.obtained` - current datetime if dataset has been downloaded or `ctime` for `"UCI HAR Dataset"` directory if dataset was already present

By default variables associated with frequency will be omitted.


```r
grep("Freq", colnames(data$tidy.data), value = TRUE)
```

```
## character(0)
```

If you want to include these in the output dataset run `main()` function with `include.freq.data=TRUE`


```r
data.with.freqs <- main(include.freq.data = TRUE)
grep("Freq", colnames(data.with.freqs$tidy.data), value = TRUE)
```

```
##  [1] "fBodyAcc-meanFreq()-X"           "fBodyAcc-meanFreq()-Y"          
##  [3] "fBodyAcc-meanFreq()-Z"           "fBodyAccJerk-meanFreq()-X"      
##  [5] "fBodyAccJerk-meanFreq()-Y"       "fBodyAccJerk-meanFreq()-Z"      
##  [7] "fBodyGyro-meanFreq()-X"          "fBodyGyro-meanFreq()-Y"         
##  [9] "fBodyGyro-meanFreq()-Z"          "fBodyAccMag-meanFreq()"         
## [11] "fBodyBodyAccJerkMag-meanFreq()"  "fBodyBodyGyroMag-meanFreq()"    
## [13] "fBodyBodyGyroJerkMag-meanFreq()"
```

